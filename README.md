# METROBEAT

## Technologies

- ReactJS
- OAuth Authentication
- Spotify API
- Ruby on Rails

## Features

- Singe Page Application
- Full OAuth Authentication for secure sign in
- Connects to the Spotify API
  - Creatively create playlists and add songs to it
  - Secure sign in for anyone with a Spotify account
- Search for and listen to any song on Spotify
- Guess tempo (BPM) of songs to add them to a playlist

## Summary

METROBEAT is the world's first gamified Spotify playlist maker! Users sign in with his/her Spotify username and password. Upon account creation, METROBEAT creates a new playlist in the user's Spotify account. Players search for and choose their favorite songs. Next, they guess the Beats Per Minute of that song. When they guess correctly, that song is added to their new playlist and can play again.

## How to Use METROBEAT

Sign in with your valid Spotify account. Search for a song you love. Once you've found it, give it a click to begin the game. Listen to the song and try to count the Beats per Minute. When you guess right, METROBEAT will add that song to your playlist, and you can search for another song.

## Known Bugs

- User sign in currently down (still in testing phase)

### Future Updates

Players will be able to:
- search by artist and genre
- play multiple game modes including guess the artist, song title, album and more
- log back in to view past games
- enjoy a more visibly pleasing experience with updated layout and styling